package com.example.graphql.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;

@Getter
@Setter
@Entity
public class Person {

    @Id()
    private long id;
    private String name;
    private String mobile;
    private String email;
    private String[] address;
}
